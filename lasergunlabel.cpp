#include "lasergunlabel.h"

#include <QFont>

/*
 * This class inherits from weponlabel
 * It gives information about the laser gun ammunition and display it.
 */

/*
 * Constructor
 * Initialize attributes and set parameters for the Text
 */
LasergunLabel::LasergunLabel(){
    ammo = 10;
    max_ammo = 10;
    clips = 30;
    reload_time = 2500;

    repaint();
    setDefaultTextColor(Qt::black);
    QFont font ("Helvetica", 14);
    font.setItalic(true);
    setFont(font);

}

/*
 * This method set text for this QGraphicstextitem
 * It's called whenever there's some changes in ammo o clips
 */
void LasergunLabel::repaint(){
    setPlainText(QString("Laser Gun\n" + QString::number(ammo) +
                         "/" + QString::number(clips)));
}

/*
 * This method increase the maximum ammunition
 * It's called in class drop
 */
void LasergunLabel::increaseMaxAmmo(){
    max_ammo += 1;
    ammo += 1;
    clips += max_ammo;
    repaint();
}
