#ifndef DROP_H
#define DROP_H

#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QObject>
#include <functional>

class Drop: public QObject, public QGraphicsPixmapItem{
public:
    /*constructor*/
    Drop();

    /*public methods*/
    void getBonus();

private:
    /*private attributes*/
    enum DROP_ID{
        HP_10,
        HP_50,
        HP_100,
        AMMO,
        POWERUP
    };
    DROP_ID drop;
};

#endif // DROP_H
