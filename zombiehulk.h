#ifndef ZOMBIEHULK_H
#define ZOMBIEHULK_H

#include "enemy.h"
#include <QObject>

class ZombieHulk: public Enemy{
    Q_OBJECT
public:
    //Constructor
    ZombieHulk();

    //Public Methods
    void increaseAD(int );
    virtual void move();
private:
    //Private Attributes
    int attack_damage;
};

#endif // ZOMBIEHULK_H
