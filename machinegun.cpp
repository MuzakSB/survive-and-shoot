#include "machinegun.h"
#include "enemy.h"
#include "zombiehulk.h"
#include "game.h"

#include <qmath.h>
#include <QBrush>
#include <QGraphicsScene>
#include <QList>

/*
 * This class rappresents one of the player's weapon.
 * Machine gun shoots a series of bullets in a strict angle, use only one ammo
 * deals medium damage but it's not very consistent
 */

extern Game *game;

/*
 * Constructor
 * draw the bullet and initialize attributes
 */
MachineGun::MachineGun(){
    setRect(0, 0, 3, 2);

    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::black);

    attack_damage = 12 + game->player->getAD();
    remove = false;
}

/*
 * This slots calculate the trajectory and move the bullet,
 * also assign damage to enemy when collided with it
 * there's a small angle offset for everybullet
 */
void MachineGun::move(){
    QList<QGraphicsItem *> colliding_items = collidingItems();
    foreach (QGraphicsItem * i, colliding_items){
        Enemy * enemy = dynamic_cast<Enemy *>(i);
        if(enemy){
            enemy->takeDamage(getAD());
            remove = true;
            return;
        }
    }
    bullet_speed = 20;
    double angle = rotation(); // in degrees
    // convert to radians, because qSin qCos use radians
    angle = qDegreesToRadians(angle);
    double dx = bullet_speed * qCos(angle);
    double dy = bullet_speed * qSin(angle);

    setPos(x() + dx , y() + dy);
    if (pos().y() < 0 || pos().y() > 720 || pos().x() < 0 || pos().x() > 1280){
        remove = true;
        return;
    }
}
