#ifndef PLAYERLABEL_H
#define PLAYERLABEL_H

#include <QGraphicsTextItem>

class PlayerLabel: public QGraphicsTextItem{
public:
    //Constructor
    PlayerLabel();

    //Public methods
    void repaint();
};

#endif // PLAYERLABEL_H
