#include "enemygun.h"
#include "player.h"
#include "game.h"

#include <qmath.h>
#include <QBrush>
#include <QGraphicsScene>
#include <QList>

/*
 * This class functions as ZombieRanged bullet
 * Inherits from Bullet.
 */

extern Game * game;

/*
 * Constructor
 * draw the bullet and initilize it's attributes
 * have a timer for moving the bullet
 */
EnemyGun::EnemyGun(){
    setRect(0, 0, 3, 3);

    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::red);
    setBrush(brush);

    attack_damage = 2 + game->getGameLv();
    remove = false;
}

/*
 * This slots calculate the trajectory and move the bullet,
 * also assign damage to player when collided with it
 */
void EnemyGun::move(){
    bullet_speed = 13;
    double angle = rotation(); // in degrees
    // convert to radians, because qSin qCos use radians
    angle = qDegreesToRadians(angle);
    double dx = bullet_speed * qCos(angle);
    double dy = bullet_speed * qSin(angle);

    QList<QGraphicsItem *> colliding_items = collidingItems();
    foreach (QGraphicsItem * i, colliding_items){
        Player * target = dynamic_cast<Player *>(i);
        if(target){
            game->player->damagePlayer(attack_damage);
            remove = true;
            return;
        }
    }
    setPos(x() + dx , y() + dy);
    //Remove when hit window borders
    if (pos().y() < 0 || pos().y() > 800 || pos().x() < 0 || pos().x() > 1200){
        remove = true;
        return;
    }
}
