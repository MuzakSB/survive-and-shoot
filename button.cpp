#include "button.h"

#include <QGraphicsTextItem>
#include <QBrush>

/*
 * This class is a custom button usable in Qgraphicsscene,
 * since Qpushbutton isn't usable in it.
 */

Button::Button(QString name, QGraphicsItem *parent):QGraphicsRectItem(parent){
    //setting up rectangle and it's style
    setRect(0, 0, 150, 70);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::gray);
    setBrush(brush);

    //set up text
    text = new QGraphicsTextItem(name, this);
    int x = rect().width() / 2 - text->boundingRect().width() / 2;
    int y = rect().height() / 2 - text->boundingRect().height() / 2;
    text->setPos(x, y);
}

/*
 * This method add clicked SIGNAL to this class
 */
void Button::mousePressEvent(QGraphicsSceneMouseEvent *event) {emit clicked();}
