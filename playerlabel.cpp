#include "playerlabel.h"
#include "game.h"

#include <QFont>

/*
 * This class displays player's stats
 * like health, attack_damage and armor.
 */

extern Game * game;

/*
 * Constructor
 * set text parameters
 */
PlayerLabel::PlayerLabel(){
    setDefaultTextColor(Qt::darkRed);
    QFont font ("Helvetica", 18);
    font.setItalic(true);
    setFont(font);
    setPlainText(QString("HEALTH : " + QString::number(100) +
                         "/" + QString::number(100) +
                         "\nSTAMINA : " + QString::number(100) +
                         "/" + QString::number(100) +
                         "\nArmor: " + QString::number(0) +
                         "\nAD: " + QString::number(0)));
}

/*
 * This method set text for this QGraphicstextitem
 * It's called whenever there's change in player's health, attack_damage or armor.
 */
void PlayerLabel::repaint(){
    setPlainText(QString("HEALTH : " + QString::number(game->player->getHp()) +
                         "/" + QString::number(game->player->getMaxHp()) +
                         "\nSTAMINA : " + QString::number(game->player->getStamina()) +
                         "/" + QString::number(game->player->getMaxStamina()) +
                         "\nArmor: " + QString::number(game->player->getArmor()) +
                         "\nAD: " + QString::number(game->player->getAD())));
}
