#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QTimer>
#include <QObject>
#include <QLineF>
#include <QList>

#include "button.h"
#include "enemy.h"
#include "bullet.h"
#include "player.h"
#include "playerlabel.h"
#include "scorelabel.h"
#include "handgunlabel.h"
#include "machinegunlabel.h"
#include "shotgunlabel.h"
#include "lasergunlabel.h"

/*
 * This class draw the canvas, function also as game engine.
 */

//Todo: add sprint in player and move all health armor and stuff to player
//Todo: use single timer for bullet, player and enemy in game instead of each one has own timer
//Todo: add autofire for machinegun (fire while key is pressed)
//Todo: collapse all weapon label in weapon label

class Game: public QGraphicsView {
    Q_OBJECT
public:
    //Contructor
    Game();

    //public attributes
    QGraphicsScene *map;
    Player *player;
    PlayerLabel *playerlabel;
    ScoreLabel *scorelabel;
    HandgunLabel *handgunlabel;
    MachinegunLabel *machinegunlabel;
    ShotgunLabel *shotgunlabel;
    LasergunLabel *lasergunlabel;
    QList<Bullet *> bullet_list;

    //public methods

    //Game's useful methods
    void mainMenu();
    void drawGui();
    void initilize();
    void gameOverMenu();
    void pauseGame();
    void resumeGame();

    //player's movement events
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void changeEvent(QEvent *event);

    //Monster spawn methods
    QPoint getSpawnCo();

    //Score methods
    void increaseScore(int );
    int getScore();
    int getGameLv();

    //Reloading getter and setter
    bool getReload();
    void setReload(bool );



private slots:
    //private slots
    void startGame();
    void restartGame();
    void spawnEnemy();
    void shootBullet();
    void updatePlayer();
    void updateEnemy();
    void updateBullet();


private:
    //private attributes
    QTimer *spawn_timer;
    QTimer *player_timer;
    QTimer *enemy_timer;
    QTimer *bullet_timer;
    int window_width = 1280;
    int window_height = 720;

    enum WeaponFlag{
        handgun,
        machinegun,
        shotgun,
        lasergun
    };
    WeaponFlag weapon_flag;

    QList<Enemy *> enemy_list;

    QLineF fire_ln;
    QLineF bullet_ln;
    int bullet_xoffset;
    int bullet_yoffset;

    int score;
    int game_lv;
    int next_lv_score;

    bool inGame = false;
    bool reloading = false;
};

#endif // GAME_H
