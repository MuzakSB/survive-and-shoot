#include "drop.h"
#include "handgun.h"
#include "game.h"

#include <QPixmap>
#include <stdlib.h>
#include <time.h>

/*
 * This class implements all game's enemy drop
 */

extern Game *game;

/*
 * Constructor
 * Draw a random drop and set the drop's flag
 */
Drop::Drop(){
    srand(time(NULL));
    int x = (rand() % 20);

    if(x>= 0 && x <6){
        drop = HP_10;
        setPixmap(QPixmap(":/drop/hp_10.png"));
        return;
    }
    else if(x>= 5 &&  x<9){
        drop = HP_50;
        setPixmap(QPixmap(":/drop/hp_50.png"));
        return;
    }
    else if(x>= 9 &&  x<11){
        drop = HP_100;
        setPixmap(QPixmap(":/drop/hp_100.png"));
    }
    else if(x>= 11 &&  x<17){
        drop = AMMO;
        setPixmap(QPixmap(":/drop/ammo.png"));
        return;
    }
    else if(x>= 17 &&  x<=19){
        drop = POWERUP;
        setPixmap(QPixmap(":/drop/powerup.png"));
        return;
    }
}

/*
 * This method gave the drop bonus to player
 * heal for 10, 50 or full if get HP_10, HP_50, HP_FULL flag
 * give all weapons armor if get AMMO flag
 * give weapon's Powerup if get PowerUp flag
 */
void Drop::getBonus(){
    switch (drop){
        case HP_10:
            game->player->healPlayer(10);
            break;
        case HP_50:
            game->player->healPlayer(50);
            break;
        case HP_100:
            game->player->healPlayer(100);
            break;
        case AMMO:
            game->handgunlabel->increaseclips();
            game->machinegunlabel->increaseclips();
            game->shotgunlabel->increaseclips();
            game->lasergunlabel->increaseclips();
            break;
        case POWERUP:
            srand(time(NULL));
            game->player->increaseMaxHP();
            game->player->increaseAD();
            game->player->increaseArmor();
            game->handgunlabel->increaseMaxAmmo();
            game->machinegunlabel->increaseMaxAmmo();
            game->shotgunlabel->increaseMaxAmmo();
            game->lasergunlabel->increaseMaxAmmo();
            break;
        default:
            break;
    }
    game->map->removeItem(this);
    delete(this);
}

