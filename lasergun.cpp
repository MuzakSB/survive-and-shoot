#include "lasergun.h"
#include "enemy.h"
#include "zombiehulk.h"
#include "game.h"

#include <qmath.h>
#include <QBrush>
#include <QGraphicsScene>
#include <QList>
#include <math.h>

/*
 * This class rappresents one of the player's weapon.
 * Laser gun deal more damage and also penetrate enemies
 * slow reload and few shots
 */

extern Game *game;

/*
 * Constructor
 * draw the bullet and initialize attributes
 */
LaserGun::LaserGun(){
    setRect(0, 0, 11, 3);

    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::blue);
    setBrush(brush);

    penetration = 2 + floor(game->getGameLv() / 2);
    attack_damage = 25 + game->player->getAD() * 5;
    remove = false;
}

/*
 * This slots calculate the trajectory and move the bullet,
 * also assign damage to enemy when collided with it
 * keeps moving if there's some penetration left
 * penetration doesn't have effect on ZombieHulk enemies
 */
void LaserGun::move(){
    QList<QGraphicsItem *> colliding_items = collidingItems();
    foreach (QGraphicsItem * i, colliding_items){
        Enemy * enemy = dynamic_cast<Enemy *>(i);
        if(enemy){
            enemy->takeDamage(getAD());
            if(dynamic_cast<ZombieHulk *>(i)) {penetration = 0;}
            else {penetration -= 1;}
            if(penetration <= 0){
                remove = true;
                return;
            }
        }
    }
    bullet_speed = 30;
    double angle = rotation(); // in degrees
    // convert to radians, because qSin qCos use radians
    angle = qDegreesToRadians(angle);
    double dx = bullet_speed * qCos(angle);
    double dy = bullet_speed * qSin(angle);
    setPos(x() + dx , y() + dy);

    if (pos().y() < 0 || pos().y() > 720 || pos().x() < 0 || pos().x() > 1280){
        remove = true;
        return;
    }
}
