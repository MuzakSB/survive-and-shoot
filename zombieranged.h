#ifndef ZOMBIEGUN_H
#define ZOMBIEGUN_H

#include "enemy.h"
#include <QObject>

class ZombieRanged: public Enemy {
    Q_OBJECT
public:
    //Constructor
    ZombieRanged();
    virtual void move();
    void shoot();

private:
    //Private attributes
    int attack_distance;
    double attack_timer;

};

#endif // ZOMBIEGUN_H
