#ifndef SCORELABEL_H
#define SCORELABEL_H

#include <QGraphicsTextItem>

/*
 * this class display map's level and player score
 * also manage all the calculation for levels
 */
class ScoreLabel: public QGraphicsTextItem{
public:
    //Constructor
    ScoreLabel();

    //Public Methods
    void repaint();


private:
    //Private Attributes


};

#endif // SCORELABEL_H
