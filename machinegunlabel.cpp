#include "machinegunlabel.h"

#include <QFont>

/*
 * This class inherits from weponlabel
 * It gives information about the machine gun ammunition and display it.
 */

/*
 * Constructor
 * Initialize attributes and set parameters for the Text
 */
MachinegunLabel::MachinegunLabel(){
    ammo = 20;
    max_ammo = 20;
    clips = 60;
    reload_time = 1500;

    repaint();
    setDefaultTextColor(Qt::black);
    QFont font ("Helvetica", 14);
    font.setItalic(true);
    setFont(font);
}

/*
 * This method set text for this QGraphicstextitem
 * It's called whenever there's some changes in ammo o clips
 */
void MachinegunLabel::repaint(){
    setPlainText(QString("Machine gun\n" + QString::number(ammo) +
                         "/" + QString::number(clips)));
}

/*
 * This method increase the maximum ammunition
 * It's called in class drop
 */
void MachinegunLabel::increaseMaxAmmo() {
    max_ammo += 1;
    ammo += 1;
    clips += max_ammo;
    repaint();
}

