#ifndef ENEMY_H
#define ENEMY_H

#include <QGraphicsPixmapItem>
#include <QObject>

class Enemy: public QObject, public QGraphicsPixmapItem {
    Q_OBJECT
public:
    //Constructor
    Enemy();

    //Public Methods
    int getHealth();
    void takeDamage(int );
    double getCX();
    double getCY();
    bool getDead();
    virtual void move();

protected:
    //Protected attributes
    double move_speed;
    double move_lag;
    int health;
    int xp;
    int droprate;
    QPoint spawn_point;
    bool isDead;
};

#endif // ENEMY_H
