#ifndef MACHINEGUNLABEL_H
#define MACHINEGUNLABEL_H

#include "weaponlabel.h"

class MachinegunLabel: public WeaponLabel{
public:
    //Constructor
    MachinegunLabel();

    //Public Methods
    virtual void repaint();
    virtual void increaseMaxAmmo();
};

#endif // MACHINEGUNLABEL_H
