#ifndef ZOMBIEMELEE_H
#define ZOMBIEMELEE_H

#include "enemy.h"
#include <QObject>

class ZombieMelee: public Enemy{
    Q_OBJECT
public:
    //Constructor
    ZombieMelee();
    virtual void move();    
private:
    //Private Attributes
    int attack_damage;
};

#endif // ZOMBIEMELEE_H
