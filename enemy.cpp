#include "enemy.h"
#include "drop.h"

#include <game.h>
#include <qmath.h>
#include <QGraphicsScene>
#include <QPixmap>
#include <QTimer>
#include <QLineF>
#include <QDebug>
#include <stdlib.h>
#include <time.h>

/*
 * This class is a parent class for every enemy in the game
 * Class who inherits from this one:
 * zombiehulk, zombiemelee, zombieranged
 */

extern Game *game;

/*
 * Constructor
 * Does nothing
 */
Enemy::Enemy(){}

/*
 * Private attribue health getter
 */
int Enemy::getHealth() {return health;}

/*
 * This methods assign damage to enemy
 * Also remove enemy if health is below 0 (enemy dies).
 * When enemy dies there's certain chance (depends on which enemy die)
 * that will spawn a drop
 */
void Enemy::takeDamage(int x){
    health -= x;
    if(health <= 0){
        game->increaseScore(xp);
        srand(time(NULL));
        int x = rand() % 100;
        if(x < droprate){
            Drop * drop = new Drop();
            drop->setPos(getCX(), getCY());
            game->map->addItem(drop);
        }
        isDead = true;
    }
}

/*
 * This class calculate and return enemy central x coordinate
 */
double Enemy::getCX() {return x() + pixmap().width() / 2;}

/*
 * This class calculate and return enemy central y coordinate
 */
double Enemy::getCY() {return y() + pixmap().height() / 2;}

/*
 * This method return enemy's isDead flag
 */
bool Enemy::getDead() {return isDead;}

/*
 * This slots move enemy whenever is called.
 * It will be overriden by any class who inherits this one.
 */
void Enemy::move(){}


