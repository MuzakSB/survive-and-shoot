#include "zombiehulk.h"
#include "enemy.h"
#include "game.h"
#include "player.h"

#include <QPixmap>
#include <QTimer>
#include <qmath.h>
#include <QDebug>

/*
 * This class rappresents one type of enemy
 * ZombieHulk it's bigger than others enemies, slower, tankier,
 * deal more damage, immune to penetration but have lesser spawn rate
 * It also have higher drop rate and gives more experiences
 * Inherits from class enemy
 */

extern Game * game;

/*
 * Constructor
 * draw Zombie Hulk
 * inilize attributes an set movement timer
 */
ZombieHulk::ZombieHulk(){
    setPixmap(QPixmap(":/enemy/zombie_hulk.png"));
    setTransformOriginPoint(x() + pixmap().width() / 2,
                            y() + pixmap().height() / 2);
    health = 25 + game->getGameLv() * 5;
    xp = 13 + game->getGameLv() * 2;
    attack_damage = 9 + game->getGameLv();
    droprate = 32 + game->getGameLv() * 3;
    move_lag = 1;
    isDead = false;
}

/*
 * This slots moves zombie hulk towards player
 * Deals damage to player whenever it's collide with him
 * also rotate zombie hulk towards player
 */
void ZombieHulk::move(){
    QList<QGraphicsItem *> colliding_items = collidingItems();
    foreach (QGraphicsItem * i, colliding_items){
        Player * target = dynamic_cast<Player *>(i);
        if(target){
            game->player->damagePlayer(attack_damage);
            return;
        }
    }

    if(move_lag < 1){
        move_lag += 0.2;
    }
    else{
        move_lag = 0;
        move_speed = 2.2 + 0.05 * game->getGameLv();
        QLineF ln (QPointF(getCX(), getCY()),
                    QPointF(game->player->getCX(), game->player->getCY()));
        double angle = -ln.angle(); // in degrees
        setRotation(angle);
        // convert to radians, because qSin qCos use radians
        angle = qDegreesToRadians(angle);
        double dx = move_speed * qCos(angle);
        double dy = move_speed * qSin(angle);
        setPos(x() + dx, y() + dy);
    }
}
