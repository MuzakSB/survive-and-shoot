#ifndef HANDGUN_H
#define HANDGUN_H

#include "bullet.h"
#include <QObject>

class HandGun: public Bullet{
    Q_OBJECT
public:
    //Constructor
    HandGun();
protected slots:
    //Protected slots
    virtual void move();
private:
    //Private attributes
    int crit_chanche;
    int crit_rate;

};

#endif // HANDGUN_H
