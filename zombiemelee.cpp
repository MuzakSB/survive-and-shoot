#include "zombiemelee.h"
#include "enemy.h"
#include "game.h"
#include "player.h"

#include <QPixmap>
#include <QTimer>
#include <qmath.h>

/*
 * This class rappresents one type of enemy
 * Zombiemelee have fastest movement between enemies
 * but little damage and health
 * Inherits from class enemy
 */

extern Game * game;

/*
 * Constructor
 * draw Zombie melee
 * inilize attributes an set movement timer
 */
ZombieMelee::ZombieMelee(){
    setPixmap(QPixmap(":/enemy/zombie.png"));
    setTransformOriginPoint(x() + pixmap().width() / 2,
                            y() + pixmap().height() / 2);
    move_lag = 1;
    health = 13 + game->getGameLv() * 2;
    xp = 10 + game->getGameLv() ;
    attack_damage = 3 + floor(game->getGameLv() /2);
    droprate = 18 + game->getGameLv() * 2;
    isDead = false;
}

/*
 * This slots moves zombie melee towards player
 * Deals damage whenever it's colliding with him
 * Also rotate zombie melee toward player
 */
void ZombieMelee::move(){
    QList<QGraphicsItem *> colliding_items = collidingItems();
    foreach (QGraphicsItem * i, colliding_items){
        Player * target = dynamic_cast<Player *>(i);
        if(target){
            game->player->damagePlayer(attack_damage);
            return;
        }
    }
    if(move_lag < 1) {move_lag += 0.2;}
    else{
        move_lag = 0;
        move_speed = 3.3 + 0.1 * game->getGameLv();
        QLineF ln (QPointF(getCX(), getCY()),
                   QPointF(game->player->getCX(), game->player->getCY()));
        double angle = -ln.angle(); // in degrees
        setRotation(angle);
        // convert to radians, because qSin qCos use radians
        angle = qDegreesToRadians(angle);
        double dx = move_speed * qCos(angle);
        double dy = move_speed * qSin(angle);
        setPos(x() + dx, y() + dy);
    }
}
