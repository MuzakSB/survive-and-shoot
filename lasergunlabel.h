#ifndef LASERGUNLABEL_H
#define LASERGUNLABEL_H

#include "weaponlabel.h"


class LasergunLabel: public WeaponLabel{
public:
    //Constructor
    LasergunLabel();

    //Public Methods
    virtual void repaint();
    virtual void increaseMaxAmmo();
};

#endif // LASERGUNLABEL_H
