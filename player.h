#ifndef PLAYER_H
#define PLAYER_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QMouseEvent>
#include <QKeyEvent>

class Player: public QObject, public QGraphicsPixmapItem {
    Q_OBJECT
public:
    //Constructor
    Player();
    //Public Attributes
    bool key_up, key_down, key_left, key_right, key_sprint;

    //Public methods
    void move();
    double getCX();
    double getCY();
    int getMaxHp();
    int getHp();
    int getArmor();
    int getAD();
    int getStamina();
    int getMaxStamina();
    bool getDead();
    void increaseMaxHP();
    void healPlayer(int );
    void damagePlayer(int );
    void increaseArmor();
    void increaseAD();
    void stopPlayer();


private:
    //Private attributes
    int health;
    int max_health;
    int stamina;
    int max_stamina;
    int armor;
    int attack_damage;
    double move_speed;
    double sprint_speed;
    double staminaRecover;
    bool isDead;
};

    #endif // PLAYER_H
