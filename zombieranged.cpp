#include "zombieranged.h"
#include "enemy.h"
#include "game.h"
#include "enemygun.h"

#include <QPixmap>
#include <QTimer>
#include <qmath.h>
#include <QDebug>

/*
 * This class rappresents one type of enemy
 * Zombieranged attack from distance
 * it stops from moving and attack player if he is in certain range
 * Inherits from class enemy
 */

extern Game *game;

/*
 * Constructor
 * draw Zombie range
 * inilize attributes an set movement timer
 */
ZombieRanged::ZombieRanged(){
    setPixmap(QPixmap(":/enemy/zombie_gun.png"));
    setTransformOriginPoint(x() + pixmap().width() / 2,
                            y() + pixmap().height() / 2);

    move_lag = 1;
    attack_timer = 0;
    health = 9 + game->getGameLv() ;
    xp = 10 + game->getGameLv();
    attack_distance = 190 + game->getGameLv() * 5;
    droprate = 18 + game->getGameLv();
    isDead = false;
}

/*
 * This slots moves zombie ranged towards player
 * Stop moving and start attacking player if it's in certain range from the him
 * Also rotate zombie ranged toward player
 */
void ZombieRanged::move() {
    QLineF ln (QPointF(getCX(), getCY()),
               QPointF(game->player->getCX(), game->player->getCY()));
    double angle = -ln.angle(); // in degrees
    setRotation(angle);
    if(ln.length() < attack_distance){
        if(attack_timer < 1) {attack_timer += 0.05;}
        else {
        attack_timer = 0;
        shoot();
        }
    }
    else{
        if(move_lag < 1) {move_lag += 0.2;}
        else{
            move_lag = 0;
            move_speed = 2.5 + 0.075 * game->getGameLv();
            // convert to radians, because qSin qCos use radians
            angle = qDegreesToRadians(angle);
            double dx = move_speed * qCos(angle);
            double dy = move_speed * qSin(angle);
            setPos(x() + dx, y() + dy);
        }
    }
}

/*
 * creating bullets set their position then move them towards player
 * also rotate zombie toward the player
 */
void ZombieRanged::shoot(){
    EnemyGun * bullet = new EnemyGun();
    game->bullet_list.append(bullet);

    double bullet_x = x() + pixmap().width() / 2 + 25;
    double bullet_y = y() + pixmap().height() / 2 + 10;

    QLineF bullet_ln(QPointF(x() + pixmap().width() / 2,
                             y() + pixmap().height()/ 2),
                     QPointF(bullet_x, bullet_y));
    QLineF fire_ln(QPointF(x() + pixmap().width() / 2,
                           y() + pixmap().height()/ 2),
                   QPointF(game->player->getCX(), game->player->getCY()));
    double final_angle = fire_ln.angle() + bullet_ln.angle();

    setRotation(-1* (fire_ln.angle()));
    bullet_ln.setAngle(final_angle);
    bullet->setPos(bullet_ln.x2(), bullet_ln.y2());
    bullet->setRotation(-1 * fire_ln.angle());

    game->map->addItem(bullet);
}
