#ifndef LASERGUN_H
#define LASERGUN_H

#include "bullet.h"

#include <QObject>


class LaserGun: public Bullet{
    Q_OBJECT
public:
    //Constructor
    LaserGun();
protected slots:
    //Protected Slots
    virtual void move();
private:
    //Private attributes
    int penetration;

};

#endif // LASERGUN_H
