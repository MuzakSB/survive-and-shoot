#include "handgunlabel.h"

#include <QFont>

/*
 * This class inherits from weponlabel
 * It give information about the handgun ammunition and display it.
 */

/*
 * Constructor
 * Initialize attributes and set parameters for the Text
 */
HandgunLabel::HandgunLabel(){
    ammo = 12;
    max_ammo = 12;
    clips = 60;
    reload_time = 1000;

    repaint();
    setDefaultTextColor(Qt::black);
    QFont font ("Helvetica", 14);
    font.setItalic(true);
    setFont(font);
}

/*
 * This method set text for this QGraphicstextitem
 * It's called whenever there's some changes in ammo o clips
 */
void HandgunLabel::repaint(){
    setPlainText(QString("Hand gun\n" + QString::number(ammo) +
                         "/" + QString::number(clips)));
}

/*
 * This method increase the maximum ammunition
 * It's called in class drop
 */
void HandgunLabel::increaseMaxAmmo(){
    max_ammo += 2;
    ammo +=2;
    clips += max_ammo;
    repaint();
}

