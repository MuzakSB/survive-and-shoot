#ifndef SHOTGUN_H
#define SHOTGUN_H

#include"bullet.h"

#include <QObject>

class ShotGun: public Bullet{
    Q_OBJECT
public:
    //Constructor
    ShotGun();
protected slots:
    //Protected Slots
    virtual void move();
private:
    //Private Attributes
    int range;
    int penetration;
    double angle_offset;
};

#endif // SHOTGUN_H
