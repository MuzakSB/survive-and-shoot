#include "game.h"
#include "player.h"
#include "handgun.h"
#include "lasergun.h"
#include "shotgun.h"
#include "machinegun.h"
#include "enemy.h"
#include "zombieranged.h"
#include "zombiemelee.h"
#include "zombiehulk.h"
#include "playerlabel.h"
#include "scorelabel.h"
#include "handgunlabel.h"

#include <QGraphicsScene>
#include <QLineF>
#include <QPointF>
#include <QTimer>
#include <QPixmap>
#include <QPushButton>
#include <qmath.h>
#include <stdlib.h>
#include <time.h>

/*
 * This class draw the canvas and game menus.
 * Funtion also as game engine.
 */
//does everything

/*
 * Game class constructor
 * draw canvas
 */
Game::Game(){
    //new map
    map = new QGraphicsScene();
    //set mapsize
    map->setSceneRect(0,0,window_width,window_height);
    map->addPixmap(QPixmap(":/background/bg.png"));

    //some scene settings
    setScene(map);
    setFixedSize(window_width, window_height);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

/*
 * initiliaze attributes
 */
void Game::initilize(){
    //initialize some variables
    bullet_xoffset = 25;
    bullet_yoffset = 11;
    score = 0;
    game_lv = 1;
    next_lv_score = 100;
    weapon_flag = handgun;//set up default weapon
    inGame = true;
    reloading = false;
}

/*
 * This method draw all game's items
 * also initiliaze game's timer.
 */
void Game::drawGui(){
    map->addPixmap(QPixmap(":/background/bg.png"));
    //create player
    player = new Player();
    player->setPos(map->width() / 2 - player->pixmap().width() / 2,
                   map->height() / 2 - player->pixmap().height() / 2);
    map->addItem(player);

    //Drawn game's label
    playerlabel = new PlayerLabel();
    playerlabel->setPos(playerlabel->x() + 10, playerlabel->y());
    playerlabel->setZValue(1);
    map->addItem(playerlabel);

    handgunlabel = new HandgunLabel();
    handgunlabel->setPos(handgunlabel->x() + 300, handgunlabel->y());
    handgunlabel->setZValue(1);
    map->addItem(handgunlabel);

    machinegunlabel = new MachinegunLabel();
    machinegunlabel->setPos(machinegunlabel->x() + 420, machinegunlabel->y());
    machinegunlabel->setZValue(1);
    map->addItem(machinegunlabel);

    shotgunlabel = new ShotgunLabel();
    shotgunlabel->setPos(shotgunlabel->x() + 570, machinegunlabel->y());
    map->addItem(shotgunlabel);

    lasergunlabel = new LasergunLabel();
    lasergunlabel->setPos(lasergunlabel->x() + 690, lasergunlabel->y());
    lasergunlabel->setZValue(1);
    map->addItem(lasergunlabel);

    scorelabel = new ScoreLabel();
    scorelabel->setPos(scorelabel->x() + 980, scorelabel->y());
    scorelabel->setZValue(1);
    map->addItem(scorelabel);

    //initiliaze spawn enemy timer and first spawn
    spawn_timer = new QTimer();
    connect(spawn_timer, SIGNAL(timeout()), this, SLOT(spawnEnemy()));
    spawn_timer->start(7000); //change back timer
    QTimer::singleShot(50,this, SLOT(spawnEnemy()));\

    //initilize and start player timer
    player_timer = new QTimer();
    connect(player_timer, SIGNAL(timeout()), this, SLOT(updatePlayer()));
    player_timer->start(25);

    //initiliaze enemy's movement timer
    enemy_timer = new QTimer();
    connect(enemy_timer, SIGNAL(timeout()), this, SLOT(updateEnemy()));
    enemy_timer->start(50);

    //initiliaze enemy's bullet timer
    bullet_timer = new QTimer();
    connect(bullet_timer, SIGNAL(timeout()), this, SLOT(updateBullet()));
    bullet_timer->start(25);
}

void Game::startGame(){
    map->clear();
    initilize();
    drawGui();
}
/*
 * This method stop all timer of the game
 */
void Game::pauseGame(){
    player_timer->stop();
    enemy_timer->stop();
    bullet_timer->stop();
}

/*
 * This method resume all timer of the game
 */
void Game::resumeGame(){
    player_timer->start(25);
    enemy_timer->start(50);
    bullet_timer->start(20);
}

/*
 * Display main menu
 */
void Game::mainMenu()
{
    //Draw panels
    QGraphicsRectItem  *extpanel = new QGraphicsRectItem(0,0,window_width,window_width);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::black);
    extpanel->setBrush(brush);
    extpanel->setOpacity(0.6);
    map->addItem(extpanel);

    QGraphicsRectItem  *intpanel = new QGraphicsRectItem(0,0,900,700);
    QBrush brush1;
    brush1.setStyle(Qt::SolidPattern);
    brush1.setColor(Qt::black);
    intpanel->setBrush(brush1);
    intpanel->setOpacity(0.8);
    int x_pan = window_width / 2 - 450;
    int y_pan = window_height / 2 - 350;
    intpanel->setPos(x_pan, y_pan);
    map->addItem(intpanel);


    // create text announcing Player's score
    QGraphicsTextItem *title_text = new QGraphicsTextItem();
    title_text->setPlainText("SURVIVE AND SHOOT");
    title_text->setDefaultTextColor(Qt::red);
    QFont font ("Helvetica", 80);
    font.setItalic(true);
    setFont(font);
    int x_score = window_width / 2 - title_text->boundingRect().width() / 2;
    int y_score = y_pan + 100;
    title_text->setPos(x_score, y_score);
    map->addItem(title_text);

    // create playAgain button
    Button *playB = new Button(QString("Play"));
    playB->setPos(window_width / 2 - playB->rect().width() / 2,
                       y_pan + 250);
    map->addItem(playB);
    connect(playB,SIGNAL(clicked()),this,SLOT(startGame()));

    // create quit button
    Button* quitB = new Button(QString("Quit"));
    quitB->setPos(window_width / 2 - quitB->rect().width() / 2, y_pan + 400);
    map->addItem(quitB);
    connect(quitB,SIGNAL(clicked()),this,SLOT(close()));
}

/*
 * Display end game window,
 * stop alla timer and display two buttons and player's score
 */
void Game::gameOverMenu(){
    //stop all motor function
    spawn_timer->stop();
    pauseGame();
    inGame = false;
    //Draw panels
    QGraphicsRectItem *extpanel = new QGraphicsRectItem(0,0,window_width,window_width);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::black);
    extpanel->setBrush(brush);
    extpanel->setOpacity(0.6);
    map->addItem(extpanel);

    QGraphicsRectItem *intpanel = new QGraphicsRectItem(0,0,700,500);
    QBrush brush1;
    brush1.setStyle(Qt::SolidPattern);
    brush1.setColor(Qt::black);
    intpanel->setBrush(brush1);
    intpanel->setOpacity(0.8);
    int x_pan = window_width / 2 - 350;
    int y_pan = window_height / 2 - 250;
    intpanel->setPos(x_pan, y_pan);
    map->addItem(intpanel);

    // create text announcing Player's score
    QGraphicsTextItem *score_text = new QGraphicsTextItem();
    score_text->setPlainText("YOU'RE DEAD!\n YOU HAVE REACHED LEVEL: "
                             + QString::number(game_lv) + "\n AND SCORED: "
                             + QString::number(score));
    score_text->setDefaultTextColor(Qt::red);
    QFont font ("Helvetica", 50);
    font.setItalic(true);
    setFont(font);
    int x_score = window_width / 2 - score_text->boundingRect().width() / 2;
    int y_score = y_pan + 100;
    score_text->setPos(x_score, y_score);
    map->addItem(score_text);

    // create playAgain button
    Button *playAgainB = new Button(QString("Play Again"));
    playAgainB->setPos(window_width / 2 - playAgainB->rect().width() / 2,
                       y_pan + 250);
    map->addItem(playAgainB);
    connect(playAgainB,SIGNAL(clicked()),this,SLOT(restartGame()));

    // create quit button
    Button* quitB = new Button(QString("Quit"));
    quitB->setPos(window_width / 2 - quitB->rect().width() / 2, y_pan + 400);
    map->addItem(quitB);
    connect(quitB,SIGNAL(clicked()),this,SLOT(close()));
}

/*
 * This method clean all items in the map and then
 * start game a new game
 */
void Game::restartGame(){
    spawn_timer->disconnect();
    player_timer->disconnect();
    enemy_timer->disconnect();
    bullet_timer->disconnect();
    bullet_list.clear();
    enemy_list.clear();
    map->clear();
    startGame();
}

//Following Player's movement methods
//With the combination of Qtimer, keyPressEvent, keyReleaseEvent
//player can do some smooth diagonal movement
//while pressing two keys at the same time

/* 
 * KeyPressEvent map which key is used
 */
void Game::keyPressEvent(QKeyEvent *event){
    if(inGame){
        switch (event->key()){
        case Qt::Key_A:
            player->key_left = true;
            break;
        case Qt::Key_S:
            player->key_down = true;
            break;
        case Qt::Key_D:
            player->key_right = true;
            break;
        case Qt::Key_W:
            player->key_up = true;
            break;
        case Qt::Key_Shift:
            player->key_sprint = true;
            break;
        }
    }
}

/*
 * This method map the keyboard for player's movement and weapon selection
 */
void Game::keyReleaseEvent(QKeyEvent *event){
    if(inGame){
        if(!event->isAutoRepeat()){
            switch(event->key()){
            case Qt::Key_A:
                player->key_left = false;
                break;
            case Qt::Key_S:
                player->key_down = false;
                break;
            case Qt::Key_D:
                player->key_right = false;
                break;
            case Qt::Key_W:
                player->key_up = false;
                break;
            case Qt::Key_Shift:
                player->key_sprint = false;
                break;
            case Qt::Key_1:
                weapon_flag = handgun;
                player->setPixmap(QPixmap(":/pg/player_handgun.png"));
                bullet_xoffset = 25;
                bullet_yoffset = 11;
                break;
            case Qt::Key_2:
                weapon_flag = machinegun;
                player->setPixmap(QPixmap(":/pg/player_machinegun.png"));
                bullet_xoffset = 25;
                bullet_yoffset = 11;
                break;
            case Qt::Key_3:
                weapon_flag = shotgun;
                player->setPixmap(QPixmap(":/pg/player_shotgun.png"));
                bullet_xoffset = 29;
                bullet_yoffset = 11;
                break;
            case Qt::Key_4:
                weapon_flag = lasergun;
                player->setPixmap(QPixmap(":/pg/player_lasergun.png"));
                bullet_xoffset = 28;
                bullet_yoffset = 11;
            }
        }
    }
}

/*
 * This method fix a little bug
 * the player keep moving when the windows lose focus
 */
void Game::changeEvent(QEvent *event){
    if(inGame){
        QWidget::changeEvent(event);
        if(event->type() == QEvent::ActivationChange){
            if(this->isActiveWindow()){
                resumeGame();
            }
            else{
                pauseGame();
            }
        }
    }
}

/*
 * This method rotate the player following the mouse's cursor
 */
void Game::mouseMoveEvent(QMouseEvent *event){
    if(inGame){
        //draw line from player to mouse point, then get the angle
        fire_ln.setPoints(QPointF(player->getCX(),player->getCY()), event->pos());
        player->setRotation(-1 * fire_ln.angle());
    }
}

/*
 * This method set the right parameter for shooting
 *
 * i wanted to start the bullet from a certain point,
 * so i draw the line from that point and the center point of the player
 * anytime i rotate the player i rotate the that line and then get the new
 * x and y coordinates and set the bullet to start from there
 *
 * when setRotation we multiply for -1 because setRotation set angle in clockwise
 * while the ln.angle is in anticlockwise
 */

void Game::mousePressEvent(QMouseEvent *event){
    if(inGame){
        double bullet_x = player->getCX() + bullet_xoffset;
        double bullet_y = player->getCY() + bullet_yoffset;

        bullet_ln.setPoints(QPointF(player->getCX(), player->getCY()),
                            QPointF(bullet_x, bullet_y));
        fire_ln.setPoints(QPointF(player->getCX(),player->getCY()), event->pos());
        double final_angle = fire_ln.angle() + bullet_ln.angle();
        player->setRotation(-1 * fire_ln.angle());
        bullet_ln.setAngle(final_angle);

        switch (weapon_flag) {
        case handgun:
            if(handgunlabel->getAmmo() > 0) {
                handgunlabel->decreaseAmmo();
                shootBullet();
                return;
            }
            else if(!reloading){
                reloading = true;
                QTimer::singleShot(handgunlabel->getReloadTime(), handgunlabel,
                                  SLOT(reload()));
                return;
            }
            break;
        case machinegun:
            if(machinegunlabel->getAmmo() > 0) {
                machinegunlabel->decreaseAmmo();
                srand (time(NULL));
                int bonus_bullet = rand() % 3 + 2;
                for(int i = 0; i < bonus_bullet; i++){
                    QTimer::singleShot(40 * i,this, SLOT(shootBullet()));
                }
                return;
            }
            else if(!reloading){
                reloading = true;
                QTimer::singleShot(machinegunlabel->getReloadTime(),
                                  machinegunlabel, SLOT(reload()));
                return;
            }
            break;
        case shotgun:
            if(shotgunlabel->getAmmo() > 0){
                shotgunlabel->decreaseAmmo();
                for (int i = 0; i < 5; i++)
                    shootBullet();
                return;
            }
            else if(!reloading){
                reloading = true;
                QTimer::singleShot(shotgunlabel->getReloadTime(), shotgunlabel,
                                  SLOT(reload()));
                return;
            }
            break;
        case lasergun:
            if(lasergunlabel->getAmmo() > 0){
                lasergunlabel->decreaseAmmo();
                if(lasergunlabel->getAmmo() > 0)
                    shootBullet();
                return;
            }
            else if(!reloading){
                reloading = true;
                QTimer::singleShot(lasergunlabel->getReloadTime(), lasergunlabel,
                                  SLOT(reload()));
                return;
            }
            break;
        default:
            break;
        }
    }
    else {
        QGraphicsView::mousePressEvent(event);
    }
}


/*
 * This method create bullet using the weapon_flag and add it to the map
 */
void Game::shootBullet(){
    Bullet *bullet = NULL;
    switch (weapon_flag) {
        case handgun:
            bullet = new HandGun();
            break;
        case machinegun:
            bullet = new MachineGun();
            break;
        case shotgun:
            bullet = new ShotGun();
            break;
        case lasergun:
            bullet = new LaserGun();
            break;
        default:
            break;
    }
    bullet_list.append(bullet);
    bullet->setPos(bullet_ln.x2(), bullet_ln.y2());
    bullet->setRotation(-1 * fire_ln.angle());
    map->addItem(bullet);
}

/*
 * This slots update player's movement and playerlabel's text
 */
void Game::updatePlayer(){
    playerlabel->repaint();
    scorelabel->repaint();
    handgunlabel->repaint();
    machinegunlabel->repaint();
    shotgunlabel->repaint();
    lasergunlabel->repaint();
    player->move();
    if(player->getDead()) {gameOverMenu();}
}

/*
 * This slots update all enemy's movement in the game
 */
void Game::updateEnemy(){
    for (int i = 0; i < enemy_list.size(); i++){
        Enemy *enemy = enemy_list.at(i);
        enemy->move();
        if(enemy->getDead()){
            map->removeItem(enemy);
            enemy_list.removeAt(i);
            delete enemy;
        }
    }
}

/*
 * This slots update all bullet's movement in the game
 */
void Game::updateBullet(){
    for (int i = 0; i < bullet_list.size(); i++){
        Bullet *bullet = bullet_list.at(i);
        bullet->move();
        if(bullet->getRemove()){
            map->removeItem(bullet);
            bullet_list.removeAt(i);
            delete bullet;
        }
    }
}

/*
 * get a random point in the border of the windows
 * if rand equal to 0 or 1 randomize the y axes from 0 to 690
 * while the x axes is 0 or 1250
 * if rand equal to 2 or 3 randomize the x axes from 0 to 1250
 * while the y axes is 0 or 690
 */
QPoint Game::getSpawnCo(){
    double x = 0;
    double y = 0;
    srand(time(NULL));
    int side = rand() % 4;
    switch (side) {
        case 0:
            x = 30;
            y = rand() %690;
            break;
        case 1:
            x = 1250;
            y = rand() %690;
            break;
        case 2:
            x = rand() % 1250;
            break;
        case 3:
            x = rand() % 1250;
            y = 690;
            break;
    default:
        break;
    }
    return QPoint(x,y);
}

/*
 * This methods spawn enemies
 * the number of enemies spawned is increased while game move foreward
 */
void Game::spawnEnemy(){
    srand(time(NULL));
    int melee_number = rand() % 3 + floor(game_lv / 2);
    srand (time(NULL));
    int ranged_number = rand() % 3 + floor(game_lv / 2);
    srand (time(NULL));
    int hulk_number = rand() % 2 + floor(game_lv / 3);
    QPoint melee_spawn = getSpawnCo();
    QPoint ranged_spawn = getSpawnCo();
    QPoint hulk_spawn = getSpawnCo();
    for(int i = 0; i < melee_number; i++){
        Enemy *enemy = new ZombieMelee();
        enemy_list.append(enemy);
        enemy->setPos(melee_spawn);
        map->addItem(enemy);
    }
    for(int i = 0; i < ranged_number; i++){
        Enemy *enemy = new ZombieRanged();
        enemy_list.append(enemy);
        enemy->setPos(ranged_spawn);
        map->addItem(enemy);
    }
    for(int i = 0; i < hulk_number; i++){
        Enemy *enemy = new ZombieHulk();
        enemy_list.append(enemy);
        enemy->setPos(hulk_spawn);
        map->addItem(enemy);
    }
}

//Following score's method
/*
 * This methods increase current score
 * Whenever score reaches a threshold it also increase game's level
 * Then recalculate the next threshold
 */
void Game::increaseScore(int x){
    score += x;
    if(score > next_lv_score){
        game_lv+=1;
        next_lv_score += 100 * game_lv;
    }
}

/*
 * Score getter
 */
int Game::getScore() {return score;}

/*
 * Game level getter
 */
int Game::getGameLv() {return game_lv;}

/*
 * Reloading getter
 */
bool Game::getReload(){
    return reloading;
}

/*
 * Reloading setter
 */
void Game::setReload(bool reload){
    reloading = reload;
}
