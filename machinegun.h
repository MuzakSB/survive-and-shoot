#ifndef MACHINEGUN_H
#define MACHINEGUN_H

#include "bullet.h"

#include <QObject>

class MachineGun: public Bullet{
    Q_OBJECT
public:
    //Constructor
    MachineGun();
protected slots:
    //Protected Slots
    virtual void move();
};

#endif // MACHINEGUN_H
