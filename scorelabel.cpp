#include "scorelabel.h"
#include "game.h"

#include <QFont>

/*
 * This class displays Player's score and game levels.
 * Game's level increase enemies spawn number and enemies stats
 * It also increase player's weapons stats
 */

extern Game *game;
/*
 * Constructor
 * Initilize attributes and set text parameters
 */
ScoreLabel::ScoreLabel(){
    setPlainText(QString("Game Level: " + QString::number(1) +
                     "\nSCORE : " + QString::number(0)));
    setDefaultTextColor(Qt::gray);
    QFont font("Helvetica", 22);
    font.setItalic(true);
    setFont(font);
}

/*
 * This method set text for this QGraphicstextitem
 * It's called whenever there's change in score or game level
 */
void ScoreLabel::repaint(){
    setPlainText(QString("Game Level: " + QString::number(game->getGameLv()) +
                     "\nSCORE : " + QString::number(game->getScore())));
}




