#ifndef BULLET_H
#define BULLET_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QTimer>

class Bullet: public QObject, public QGraphicsRectItem {
    Q_OBJECT
public:
    /*constructor*/
    Bullet();

    /*public attributes*/
    int getAD();
    bool getRemove();
    virtual void move();

protected:
    /*protected attributes*/
    bool remove;
    int attack_damage;
    double bullet_speed;
};

#endif // BULLET_H
