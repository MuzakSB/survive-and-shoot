#include <QApplication>
#include <QGraphicsScene>

#include "game.h"

Game * game;

/*
 * Main class
 */
int main(int argc, char *argv[]){
    QApplication a(argc, argv);
    //create game
    game = new Game();
    game->show();
    game->mainMenu();

    return a.exec();
}
