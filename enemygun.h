#ifndef ENEMYGUN_H
#define ENEMYGUN_H

#include "bullet.h"
#include <QObject>

class EnemyGun: public Bullet{
    Q_OBJECT
public:
    //Constructor
    EnemyGun();

protected slots:
    //Protected Slots
    virtual void move();

};

#endif // ENEMYGUN_H
