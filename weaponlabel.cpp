#include "weaponlabel.h"
#include "game.h"

#include<QTimer>

/*
 * This class is a parent class for every weapon's label in the game
 * Class who inherits from this one:
 * handgun label, machingun label, shotgun label, lasergun label
 */

extern Game *game;
/*
 * Constructor
 * Does nothing
 */
WeaponLabel::WeaponLabel() {}

/*
 * This methods decrease ammo whenever a shoot is fired
 * if ammo reaches 0 it call reload slots, every weapon have own reload time
 */
void WeaponLabel::decreaseAmmo() {ammo -= 1;}

/*
 * This method is virtual, it increase weapon's maximum ammunition
 * It will be overriden by every class who inherits it
 */
void WeaponLabel::increaseMaxAmmo() {}

/*
 * This class replenish clips
 * It's called in class drop
 */
void WeaponLabel::increaseclips() {clips += max_ammo;}

/*
 * Ammo getter
 */
int WeaponLabel::getAmmo() {return ammo;}

/*
 * Clips getter
 */
int WeaponLabel::getClips() {return clips;}

/*
 * Reload timer getter
 */
int WeaponLabel::getReloadTime() {return reload_time;}

/*
 * Virtual class that refreshes label's text whenevers it's called
 * It will be overriden
 */
void WeaponLabel::repaint() {}

/*
 * This method replenish ammo and decrease clips
 */
void WeaponLabel::reload(){
    if(max_ammo >= clips){
        ammo = clips;
        clips = 0;
    }
    else {
        ammo = max_ammo;
        clips -= max_ammo;
    }
    game->setReload(false);
}
