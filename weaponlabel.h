#ifndef WEAPONLABEL_H
#define WEAPONLABEL_H

#include <QGraphicsTextItem>
#include <QObject>

class WeaponLabel: public QGraphicsTextItem{
    Q_OBJECT
public:
    //Constructor
    WeaponLabel();

    //Public Methods
    void decreaseAmmo();
    void increaseMaxAmmo();
    void increaseclips();
    int getAmmo();
    int getClips();
    int getReloadTime();
    virtual void repaint();

public slots:
    //Protected Slots
    void reload();

protected:
    //Private Attributes
    int clips;
    int ammo;
    int max_ammo;
    int reload_time;
};

#endif // WEAPONLABEL_H
