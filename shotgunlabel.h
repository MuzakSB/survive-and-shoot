#ifndef SHOTGUNLABEL_H
#define SHOTGUNLABEL_H

#include "weaponlabel.h"

class ShotgunLabel: public WeaponLabel{
public:
    //Constructor
    ShotgunLabel();

    //Private Methods
    virtual void repaint();
    virtual void increaseMaxAmmo();
};

#endif // SHOTGUNLABEL_H
