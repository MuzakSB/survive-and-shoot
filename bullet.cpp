#include "bullet.h"

#include <qmath.h>
#include <QBrush>
#include <QGraphicsScene>
#include <QList>

/*
 * This is a parent class for every bullet in the game
 * every weapon is threated as a bullet
 * Class whom inherits from this one:
 * enemygun, handgun, machinegun, shotgun and lasergun.
 */

/*
 * Constructor
 * actually do nothing
 */
Bullet::Bullet(){}

/*
 * Bullet Attack damage getter
 */
int Bullet::getAD() {return attack_damage;}

/*
 * Remove getter
 */
bool Bullet::getRemove() {return remove;}

/*
 * This method move bullets whenever is called.
 * It will be overriden by any class who inherits this one.
 */
void Bullet::move(){}



