#include "player.h"
#include "drop.h"
#include "game.h"

#include <QPixmap>
#include <QTimer>
#include <QLineF>
#include <QPointF>
/*
 * This class rappresents player as Pixmap and moves him
 */

extern Game *game;
/*
 * Constructor
 * Set Pixmap
 * Initialize some attributes
 * Set up move timer
 */
Player::Player(){
    setPixmap(QPixmap(":/pg/player_handgun.png"));

    key_down = false;
    key_left = false;
    key_right = false;
    key_up = false;
    key_sprint = false;
    isDead = false;
    health = 100;
    max_health = 200;
    attack_damage = 0;
    armor = 0;
    stamina = 100;
    max_stamina = 100;
    move_speed = 2.0;
    sprint_speed = 3.5;
    staminaRecover = 0;

    setTransformOriginPoint(pixmap().width() / 2, pixmap().height() / 2);

}

/*
 * get player's center x
 */
double Player::getCX(){
    return (this->x() + pixmap().width() / 2);
}

/*
 * get player's center y
 */
double Player::getCY(){
    return (this->y() + pixmap().height() / 2);
}

/*
 * Max health getter
 */
int Player::getMaxHp() {return max_health;}

/*
 * Health getter
 */
int Player::getHp() {return health;}

/*
 * Armor getter
 */
int Player::getArmor() {return armor;}

/*
 * Attack damage getter
 */
int Player::getAD() {return attack_damage;}

/*
 * Stamina getter
 */
int Player::getStamina() {return stamina;}

/*
 * Max Stamina getter
 */
int Player::getMaxStamina() {return max_stamina;}

/*
 * Player dead flag getter
 */
bool Player::getDead() {return isDead;}

/*
 * This function actually move the player
 * check the boolean of which key is pressed
 * then update the movement
 * Also check collision detection for drop and game's window margin
 */
void Player::move(){
    int mx = 0;
    int my = 0;
    QList<QGraphicsItem *> colliding_items = collidingItems();
    foreach (QGraphicsItem * i, colliding_items){
        Drop * drop = dynamic_cast<Drop *>(i);
        if(drop){
            drop->getBonus();
        }
    }
    if(key_left && pos().x() > 10)
        key_sprint && stamina > 0 ? mx -= sprint_speed : mx -= move_speed;
    else if(key_right && pos().x() < 1230)
        key_sprint && stamina > 0 ? mx += sprint_speed : mx += move_speed;
    if(key_up && pos().y() > 10)
        key_sprint && stamina > 10 ? my -= sprint_speed : my -= move_speed;
    else if(key_down && pos().y() < 700)
        key_sprint && stamina > 0 ? my += sprint_speed : my += move_speed;

    if(key_sprint && (mx != 0 || my != 0) && stamina > 0 ) {stamina -= 1;}
    else if(stamina < max_stamina){
        if(staminaRecover < 1) {
            mx != 0 || my != 0 ? staminaRecover += 0.1 : staminaRecover += 0.2;}
        else{
            staminaRecover = 0;
            stamina +=1;
        }
    }
    setPos(x() + mx, y() + my);
}

/*
 * Increases Player's max health,
 * heal player by that ammoutn
 */
void Player::increaseMaxHP(){
    max_health += 10;
    healPlayer(10);
}

/*
 * Increase player's health and stamina
 * Called whenever player pick up heal drop or increases maxhealth
 */
void Player::healPlayer(int x){
    if((health + x) >= max_health) {health = max_health;}
    else {health += x;}
    if((stamina + x) >= max_stamina) {stamina = max_stamina;}
    else {stamina += x;}
}

/*
 * decrease player's health
 * called whenever player take damage
 * if health get below 0 call gameOver
 */
void Player::damagePlayer(int x){
    if( armor >  x) {x = -1;}
    else {x -= armor;}
    health -= x;
    if(health <= 0) {
        health = 0;
        isDead = true;}
}

/*
 * Increase Player's armor
 */
void Player::increaseArmor() {armor += 1;}

/*
 * Increase Player's attack damage
 */
void Player::increaseAD(){
    attack_damage +=1;
}

/*
 * This class reset all player's movent key
 */
void Player::stopPlayer(){
    key_up = false;
    key_down = false;
    key_left = false;
    key_right = false;
    key_sprint = false;
}

