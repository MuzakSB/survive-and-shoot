#include "shotgun.h"
#include "enemy.h"
#include "zombiehulk.h"
#include "game.h"

#include <qmath.h>
#include <QBrush>
#include <QGraphicsScene>
#include <QList>
#include <math.h>
#include <QDebug>

/*
 * This class rappresents one of the player's weapon.
 * Shot gun shots 5 bullet in a random angle and use only one ammunition
 * The bullets have a good damage but fades in a certain range
 * every bullets have little penetration
 */

extern Game *game;

/*
 * Constructor
 * draw the bullet and initialize attributes
 */
ShotGun::ShotGun(){
    setRect(0, 0, 2, 2);
    int lv = game->getGameLv();

    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::black);
    setBrush(brush);

    angle_offset = rand() % 7;
    penetration = 1 + floor(lv/3) ;
    range = 190 + lv * 10;
    attack_damage = 20 + game->player->getAD() * 3;
    remove = false;
}

/*
 * This slots calculate the trajectory and move the bullet,
 * also assign damage to enemy when collided with it
 * Every bullet is randomized in 90 degrees
 * keeps moving if there's some penetration left
 * penetration doesn't have effect on ZombieHulk enemies
 */
void ShotGun::move(){
    QList<QGraphicsItem *> colliding_items = collidingItems();
    foreach (QGraphicsItem * i, colliding_items){
        Enemy * enemy = dynamic_cast<Enemy *>(i);
        if(enemy){
            enemy->takeDamage(getAD());
            if(dynamic_cast<ZombieHulk *>(i)) {penetration = 0;}
            else {penetration -= 1;}
            if(penetration <= 0){
                remove = true;
                return;
            }
        }
    }
    bullet_speed = 15;

    double angle = rotation(); // in degrees
    angle = angle - 30 + angle_offset * 10;
    // convert to radians, because qSin qCos use radians
    angle = qDegreesToRadians(angle);
    double dx = bullet_speed * qCos(angle);
    double dy = bullet_speed * qSin(angle);
    setPos(x() + dx , y() + dy);
    range -= sqrt((pow(dx,2) + pow(dy,2)));
    if(range <= 0){
        remove = true;
        return;
    }
    //remove bullets when hit game window border
    if (pos().y() < 0 || pos().y() > 720 || pos().x() < 0 || pos().x() > 1280){
        remove = true;
        return;
    }
}
