#-------------------------------------------------
#
# Project created by QtCreator 2017-09-16T16:16:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SurviveAndShoot
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
    player.cpp \
    game.cpp \
    bullet.cpp \
    enemy.cpp \
    zombiemelee.cpp \
    zombieranged.cpp \
    handgun.cpp \
    enemygun.cpp \
    scorelabel.cpp \
    drop.cpp \
    lasergun.cpp \
    shotgun.cpp \
    machinegun.cpp \
    zombiehulk.cpp \
    handgunlabel.cpp \
    weaponlabel.cpp \
    machinegunlabel.cpp \
    shotgunlabel.cpp \
    lasergunlabel.cpp \
    playerlabel.cpp \
    button.cpp

HEADERS += \
    player.h \
    game.h \
    bullet.h \
    enemy.h \
    zombiemelee.h \
    zombieranged.h \
    handgun.h \
    enemygun.h \
    scorelabel.h \
    drop.h \
    lasergun.h \
    shotgun.h \
    machinegun.h \
    zombiehulk.h \
    handgunlabel.h \
    weaponlabel.h \
    machinegunlabel.h \
    shotgunlabel.h \
    lasergunlabel.h \
    playerlabel.h \
    button.h

FORMS +=

RESOURCES += \
    rsc/res.qrc \


DISTFILES +=
