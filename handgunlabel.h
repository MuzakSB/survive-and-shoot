#ifndef HANDGUN_LABEL_H
#define HANDGUN_LABEL_H

#include "weaponlabel.h"

class HandgunLabel: public WeaponLabel{
public:
    //Constructor
    HandgunLabel();

    //Public methods
    virtual void repaint();
    virtual void increaseMaxAmmo();
};

#endif // HANDGUN_LABEL_H
