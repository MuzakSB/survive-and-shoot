#include "shotgunlabel.h"

#include <QFont>

/*
 * This class inherits from weponlabel
 * It give information about the shotgun ammunition and display it.
 */

/*
 * Constructor
 * Initialize attributes and set parameters for the Text
 */
ShotgunLabel::ShotgunLabel(){
    ammo = 6;
    max_ammo = 6;
    clips = 36;
    reload_time = 2000;

    repaint();
    setDefaultTextColor(Qt::black);
    QFont font ("Helvetica", 14);
    font.setItalic(true);
    setFont(font);
}

/*
 * This method set text for this QGraphicstextitem
 * It's called whenever there's some changes in ammo o clips
 */
void ShotgunLabel::repaint(){
    setPlainText(QString("Shot gun\n" + QString::number(ammo) +
                         "/" + QString::number(clips)));
}

/*
 * This method increase the maximum ammunition
 * It's called in class drop
 */
void ShotgunLabel::increaseMaxAmmo(){
    max_ammo += 3;
    ammo += 3;
    clips += max_ammo;
    repaint();
}

