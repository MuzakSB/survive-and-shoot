#include "handgun.h"
#include "enemy.h"
#include "game.h"

#include <qmath.h>
#include <QBrush>
#include <QGraphicsScene>
#include <QList>

/*
 * This class rappresents one of the player's weapon.
 * handgun is the default weapon it does the lowest damage
 * but have a critical chance to increase damage
 */

extern Game *game;

/*
 * Constructor
 * draw the bullet and initialize attributes
 */
HandGun::HandGun(){
    setRect(0, 0, 3, 2);

    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::black);
    setBrush(brush);

    crit_chanche = 19 + game->getGameLv();
    crit_rate = 2 + floor(game->getGameLv() / 3);
    attack_damage = 9 + game->player->getAD() * 2;

    srand(time(NULL));
    int chanche = rand() % 100 + 1;
    if(chanche < crit_chanche) {attack_damage *= crit_rate;}
    remove = false;
}

/*
 * This slots calculate the trajectory and move the bullet,
 * also assign damage to enemy when collided with it
 */
void HandGun::move(){
    QList<QGraphicsItem *> colliding_items = collidingItems();
    foreach (QGraphicsItem * i, colliding_items){
        Enemy * enemy = dynamic_cast<Enemy *>(i);
        if(enemy){
            enemy->takeDamage(getAD());
            remove = true;
            return;
        }
    }
    bullet_speed = 17;
    double angle = rotation(); // in degrees
    // convert to radians, because qSin qCos use radians
    angle = qDegreesToRadians(angle);
    double dx = bullet_speed * qCos(angle);
    double dy = bullet_speed * qSin(angle);
    setPos(x() + dx , y() + dy);
    //remove when hit window borders
    if (pos().y() < 0 || pos().y() > 720 || pos().x() < 0 || pos().x() > 1280){
        remove = true;
        return;
    }
}


